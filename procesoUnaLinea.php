<?php
  require_once "claseCalculo.php";
  require_once "validacion.php";

  $calcular = new Calculadora();
  $validador = new Validacion();

  $valor=$_POST['operacion'];
  $valor= $validador -> validar($valor);
  if ($valor == "Error") {
    echo "Syntax Error.\n";
    return;
  }
  echo $valor," = ";
  echo $calcular -> calculoUnalinea($valor);
 ?>
