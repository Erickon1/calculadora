<?php
class Calculadora{

  public function calcularDatos($val1,$val2,$opcion){
    switch ($opcion) {
      case 'suma':
        return $val1 + $val2;
        break;
      case 'resta':
        return $val1 - $val2;
        break;
      case 'division':
         return $val1 / $val2;
         break;
      case 'mult':
        return $val1 * $val2;
        break;
      default:
        return $val1 + $val2;
        break;
    }

  }

  public function calculoUnalinea($operacion){
    #quita espacios
    $operacion = preg_replace('/\s+/', '', $operacion);
    #*
    while( preg_match("/[*]/", $operacion)){
      preg_match('/([0-9]+)+([*]+)([0-9]+)+/', $operacion, $m );
      $res = $this->opera($m[0]);
      $operacion = str_replace($m[0], $res, $operacion);
    }
    #/
    while( preg_match("/[\/]/", $operacion)){
      preg_match('/([0-9]+)+([\/]+)([0-9]+)+/', $operacion, $m );
      $res = $this->opera($m[0]);
      $operacion = str_replace($m[0], $res, $operacion);
    }
    #+
    while( preg_match("/[+]/", $operacion)){
      preg_match('/([0-9]+)+([+]+)([0-9]+)+/', $operacion, $m );
      $res = $this->opera($m[0]);
      $operacion = str_replace($m[0], $res, $operacion);
    }
    #-
    while( preg_match("/[-]/", $operacion)){
      preg_match('/([0-9]+)+([-]+)([0-9]+)+/', $operacion, $m );
      $res = $this->opera($m[0]);
      $operacion = str_replace($m[0], $res, $operacion);
    }
    return $operacion;
  }


  private function opera($operacion){
    $keywords = preg_split("/[+*\/-]/", $operacion);

    if( preg_match("/[*]/", $operacion)){
      return $keywords[0] * $keywords[1];
    }
    if( preg_match("/[+]/", $operacion)){
      return $keywords[0] + $keywords[1];
    }
    if( preg_match("/[\/]/", $operacion)){
      return $keywords[0] / $keywords[1];
    }
    if( preg_match("/[-]/", $operacion)){
      return $keywords[0] - $keywords[1];
    }

  }

}

 ?>
